# End Survival Custom Recipes Datapack
A datapack for Minecraft 1.17 to work with the Better End mod for Fabric that I use in my End Survival YouTube series

## Installation Help When Creating a New World
1. Download the datapack from my website: https://binaryvigilante.com/downloads/end-survival-custom-recipes-datapack/
2. When creating a new world, press the "Datapacks" button
3. Click "Open Folder"
4. Now drag and drop the zip file you downloaded into the folder that you just opened
5. Go back to Minecraft
6. Click the arrow when hovering over the datapack so it goes to the right
7. Click "Done"
8. You are good to go :)

## Installation Help If You Already Generated a World
1. Download the datapack from my website: https://binaryvigilante.com/downloads/end-survival-custom-recipes-datapack/
2. Navigate to your `.minecraft` folder
3. Go to the `saves` folder
4. Go to the folder containing the name of your world
5. Go to the `datapacks` folder
6. Copy and paste the zip file you downloaded to this folder
7. Done :)
